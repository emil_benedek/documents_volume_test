require_relative 'centerpiece'
require 'zip'
require 'net/sftp'
require 'net/ssh'

class SftpIntake < Centerpiece
  def initialize
    super
  end

  def sftp_transfer
    puts 'Archive will be transfered...'
    Net::SSH.start(@address, @auth_mail, password: @auth_token) do |ssh|
      ssh.sftp.connect do |sftp|
        sftp.upload!(@zip_file_name, "#{@path}#{@zip_file_name}")
      end
    end
    puts 'Archive transfered successfully.'
  end

  def zip_from_folder_contents
    delete_relative
    Zip::File.open(@zip_file_name, Zip::File::CREATE) do |zipfile|
      Dir[File.join(@temp_folder, '*')].each do |file|
        zipfile.add(file.sub(@temp_folder, ''), file)
      end
    end
    puts "/temp was zipped into #{@zip_file_name}"
  end

  def join_threads(threads_array)
    threads_array.each(&:join)
  end

  def flow(leaf_class)
    threads = []
    puts 'Files are being copied and modified...'
    FileUtils.rm_rf(Dir.glob(@temp_folder + '/*.*'))
    directory = Dir[@test_files_root + '/*.*']
    Dir[File.join(@test_files_root, '**', '*')].count { |file| File.file?(file) }
    (1..Integer(@nr_of_files, 10)).each do |i|
      directory.each do |filename|
        FileUtils.cp(filename,
                     @temp_folder + '/doc_' + i.to_s + '_' + File.basename(filename))
      end

      threads << Thread.new do
        leaf_class.threadable_replace_placeholders(i)
      end
    end
    threads.each(&:join)
    puts ''
    puts 'All files copied and modified.'
    zip_from_folder_contents

    sftp_transfer
  end

  def delete_relative
    File.delete(@zip_file_name) unless !(File.exist? @zip_file_name)
  end
end
