require_relative '../sftpintake'
require 'ffaker'

class Aon < SftpIntake
  def initialize
    super
  end

  def threadable_replace_placeholders(count)
    doc_id = SecureRandom.uuid.to_s
    #Configure UPData
    replaced = File.read("./temp/doc_#{count}_updata.xml")
  					            .gsub('<<DOC_ID>>', doc_id)
  					            .gsub('<<INDEX>>', count.to_s)
    replaced = replaced.gsub('<<EMAIL>>', @email)
    replaced = replaced.gsub('<<LANGUAGE>>', @language)
    replaced = replaced.gsub('<<RECEIVER>>', @receiver_id)
    replaced = replaced.gsub('<<BRAND>>', @brand)
    replaced = replaced.gsub('<<TODAY>>', Time.now.strftime('%Y-%m-%d'))
    replaced = replaced.gsub('<<PAYMENT_METHOD>>', @payment_method)
    replaced = replaced.gsub('<<DELIVERY>>', @delivery)
    File.open("./temp/doc_#{count}_updata.xml", 'w') {|file| file.puts replaced}
    print '*'
  end
end
