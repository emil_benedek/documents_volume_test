require_relative 'centerpiece'

class CurlIntake < Centerpiece
  def initialize
    super
    @command = 'curl '
  end

  def compose(curl_parameter)
    @command += curl_parameter
  end
end
