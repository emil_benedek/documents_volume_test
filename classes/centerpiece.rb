require_relative 'main_logger.rb'
require 'fileutils'
require 'json'

class Centerpiece
  def initialize
    @temp_folder = './temp/'
    begin
      options = fetch_options
      @nr_of_files = options['nr_of_files']
      @address = options['address']
      @auth_mail = options['auth_mail']
      @auth_token = options['auth_token']
      @path = options['path']
      @email = options['email']
      @delivery = options['delivery']
      @language = options['language']
      @zip_file_name = options['zip_file_name']
      @receiver_id = options['receiver_id']
      @brand = options['brand']
      @owner = options['owner']
      @sender_name = options['sender_name']
      @test_files_root = options['test_files_root']
      @payment_method = options['payment_method']
    rescue
      raise SystemExit
    end
  end

  def error_msg_exit
    MainLogger.logger.error 'Fatal error, exiting...'
    exit
	end

  def fetch_options
    file = File.read('./json_config.json')
    JSON.parse(file)
  end
end
