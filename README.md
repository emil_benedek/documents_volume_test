##User Guide

####Easy Setup

1. Clone the project from Bitbucket to desired folder
2. Run ```bundle install```

####How to Use

1. Modify ```json_config.json``` depending on the type of intake
	1. Not all parameters in the JSON are necessary for all intakes
	2. If uncertain about what should you write check examples under ```/test_files/json_examples```
2. In command line, go to the project root directory, should be ```documents_volume_test```
3. Run ```ruby start.rb```

####Implementation Details

##List of JSON Attributes
* ```"nr_of_files": "5"``` => how many documents should the batch contain
* ```"address": "sftp-1-2.nxt.uat.unifiedpost.com"``` => sftp address
* ```"auth_mail": "upcloud_internal_intake@intake.up-nxt.com"```
* ```"auth_token": "upc_up1int3rnal"``` => email token used for sftp authentication
* ```"path": "/DLL/BE/Documents/"``` => path to the sftp directory; must end with ```/```
* ```"email": "emil.benedek+volume123@unifiedpost.com"``` => receiver email
* ```"delivery": "Link"``` => delivery channel ```Link/Attach```
* ```"zip_file_name": "TowerFall.zip"``` => name of the zip file generated
* ```"receiver_id": "ebt2018test"``` => ReceiverID in updata; SupplierAssignedAccountID in ubl
* ```"brand": "DLL"``` => brand from updata
* ```"owner": "DLL"``` => owner from updata
* ```"sender_name": "DLL_BE"``` => Sender Name from updata
* ```"test_files_root": "./dll_ssp"``` => when module will be extended it wil support other intake types by writing another thread function
