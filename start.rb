require 'require_all'
require_all 'classes/sftp'
require_relative 'classes/centerpiece.rb'

class Runnable
  def start
		  c = Centerpiece.new
		  options = c.fetch_options

		  case options['test_files_root']
  		when './test_files/sftp/agro'
  		  a = Agro.new
  		when './test_files/sftp/aon'
  			 a = Aon.new
  		when './test_files/sftp/dll_ssp'
  			 a = Dll.new
  			 a.flow(a)
  		when './test_files/sftp/eon'
  			 a = Eon.new
  		when './test_files/sftp/syncasso'
  		  a = Syncasso.new
  		when './test_files/sftp/taurus'
  			 a = Taurus.new
  		else
  			 puts 'test_files_root from json_config.json has an invalid value...'
  			 puts 'aborting script...'
  			 exit
  		end
		
		  a.flow(a)
  end
end

a = Runnable.new
a.start
